🚀 Get started here
Esta é uma API desenvolvida em Node.js com Express, utilizando MongoDB Atlas como banco de dados. A API roda localmente no endereço http://localhost:3000. Ela é voltada para a manipulação de dados de jogos e implementa operações CRUD (Create, Read, Update, Delete), além de suportar operações PATCH. A API também inclui middleware para validação de dados e tratamento de erros.

Base URL: http://localhost:3000

Documentação Postman: https://documenter.getpostman.com/view/15534443/2sA3dvjsX1
