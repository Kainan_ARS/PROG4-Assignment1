import mongoose from 'mongoose';

const gameSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    genre: {
        type: String,
        required: true
    },
    platform: {
        type: String,
        required: true
    }
}, { collection: 'games' });


const Game = mongoose.model('Game', gameSchema);

export default Game;