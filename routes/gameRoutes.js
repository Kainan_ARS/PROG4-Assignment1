import express from 'express';
import Game from '../models/game.js';
import validateInputs from '../middleware/validation.js';
import { body } from 'express-validator';

const router = express.Router();

 
router.post('/game', [
    body('name').notEmpty(), 
    body('description').notEmpty(),
    body('genre').notEmpty(),
    body('platform').notEmpty()
], validateInputs, async (req, res) =>{
    try {
        const game = new Game(req.body);
        await game.save();
        return res.status(201).json({ game, message: 'Game registered successfully' });
    } catch (error) {
        res.status(400).send(error);
    }
});

router.get('/game/:id', async (req, res) => {
    try {
        const game = await Game.findById(req.params.id);
        if(!game){
            return res.status(404).send('The selected game was not found.');
        }
        res.status(200).send(game);

    } catch (error) {
        res.status(500).send(error);
    }
});


router.put('/game/:id', [
    body('name').notEmpty(), 
    body('description').notEmpty(),
    body('genre').notEmpty(),
    body('platform').notEmpty()
], validateInputs, async (req, res) => {
    try {
        const game = await Game.findByIdAndUpdate(
            req.params.id, 
            req.body, 
            {new: true, runValidators: true});
            if (!game){
                return res.status(404).send('The selected game was not found.');
            }
            res.status(200).json({ game, message: 'Game updated successfully' });
        } catch (error) {
        res.status(400).send(error);
    }
});

router.delete('/game/:id', async (req, res) => {
    try {
        const game = await Game.findByIdAndDelete(req.params.id);
        if(!game){
            return res.status(404).send('The selected game was not found.');
        }
        return res.status(204).send('Game deleted successfully');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.patch('/game/:id', async (req, res) => {
    try {
        const game = await Game.findByIdAndUpdate(
            req.params.id, 
            req.body, 
            {new: true, runValidators: true});
            if (!game){
                return res.status(404).send('The selected game was not found.');
            }
            res.status(200).json({ game, message: 'Game updated successfully' });
        } catch (error) {
        res.status(400).send(error);
    }
});

export default router;