function errorHandler(err, req, res, next) {

  const message = err.message || 'Erro interno no servidor';

  res.status(5000).json({ error: message });
};

export default errorHandler;
